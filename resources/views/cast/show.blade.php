@extends('layout.master')

@section('judul')
Detail Cast {{$cast->nama}}
@endsection

@section('content')

<h5>Nama    : {{$cast->nama}}</h5>
<p>Umur     : {{$cast->umur}}</p>
<p>Bio      : {{$cast->bio}}</p>

@endsection