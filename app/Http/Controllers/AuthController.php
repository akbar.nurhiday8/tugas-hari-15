<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register(){
        return view('halaman.form');
    }

    public function welcome(Request $request){
        // dd($request->all());
        
        $nama1 = $request->nama1;
        $nama2 = $request->nama2;
        $gender = $request->gender;
        $national = $request->national;
        $bahasa = $request->bhsindo;
        $biodata = $request->bio1;
        return view('halaman.welcome', compact('nama1','nama2'));
    }
}
