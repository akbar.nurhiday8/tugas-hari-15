<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/register','AuthController@register');

Route::post('/welcome','AuthController@welcome');

Route::get('/home','IndexController@home');

Route::get('/data-table',function(){
    return view ('table.datatable');
});

Route::get('table', function(){
    return view ('table.table');
});

//CRUD Genre
Route::get('/genre/create','GenreController@create');
Route::post('/genre','GenreController@store');

Route::get('/genre','GenreController@index');

Route::get('/genre/{genre_id}','GenreController@show');

Route::get('/genre/{genre_id}/edit','GenreController@edit');

Route::put('/genre/{genre_id}','GenreController@update');

Route::delete('/genre/{genre_id}','GenreController@destroy');

//CRUD Cast
Route::get('/cast','CastController@index');
Route::get('/cast/create','CastController@create');
Route::post('/cast','CastController@store');
Route::get('/cast/{cast_id}','CastController@show');
Route::get('/cast/{cast_id}/edit','CastController@edit');
Route::put('/cast/{cast_id}','CastController@update');
Route::delete('/cast/{cast_id}','CastController@destroy');